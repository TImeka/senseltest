/******************************************************************************************
* MIT License
*
* Copyright (c) 2013-2017 Sensel, Inc.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
******************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef WIN32
    #include <windows.h>
#else
    #include <pthread.h>
#endif
#include "sensel.h"
#include "sensel_device.h"

static const char* CONTACT_STATE_STRING[] = { "CONTACT_INVALID","CONTACT_START", "CONTACT_MOVE", "CONTACT_END" };
static bool enter_pressed = false;

float x_cod = 0.0f;
float y_cod = 0.0f;
float x_p = 0.0f;
float y_p = 0.0f;

void * waitForEnter()
{
    getchar();
    enter_pressed = true;
    return 0;
}

int main(int argc, char **argv)
{
	//Handle that references a Sensel device
	SENSEL_HANDLE handle = NULL;
	//List of all available Sensel devices
	SenselDeviceList list;
	//SenselFrame data that will hold the contacts
	SenselFrameData *frame = NULL;

	SenselContact *cont = NULL;

	//Get a list of avaialble Sensel devices
	senselGetDeviceList(&list);
	if (list.num_devices == 0)
	{
		fprintf(stdout, "No device found\n");
		fprintf(stdout, "Press Enter to exit example\n");
		getchar();
		return 0;
	}

	//Open a Sensel device by the id in the SenselDeviceList, handle initialized 
	senselOpenDeviceByID(&handle, list.devices[0].idx);

	//Set the frame content to scan contact data
	senselSetFrameContent(handle, FRAME_CONTENT_CONTACTS_MASK);
	//Allocate a frame of data, must be done before reading frame data
	senselAllocateFrameData(handle, &frame);
	//Start scanning the Sensel device
    senselStartScanning(handle);
    
	//マスクを選択 sensel.h 136~171
	senselSetContactsMask(handle, CONTACT_MASK_PEAK);

    fprintf(stdout, "Press Enter to exit example\n");
    #ifdef WIN32
        HANDLE thread = CreateThread(NULL, 0, waitForEnter, NULL, 0, NULL);
    #else
        pthread_t thread;
        pthread_create(&thread, NULL, waitForEnter, NULL);
    #endif
    
    while (!enter_pressed)
    {
		unsigned int num_frames = 0;
		//Read all available data from the Sensel device
		senselReadSensor(handle);
		//Get number of frames available in the data read from the sensor
		senselGetNumAvailableFrames(handle, &num_frames);
		for (int f = 0; f < num_frames; f++)
		{
			//Read one frame of data
			senselGetFrame(handle, frame);

			//Print out contact data
			if (frame->n_contacts > 0) {
				
				//当たっている数
				//fprintf(stdout, "Num Contacts: %d\n", frame->n_contacts);

				for (int c = 0; c < frame->n_contacts; c++)
				{
					unsigned int state = frame->contacts[c].state;

					//IDと状態
					//fprintf(stdout, "Contact ID: %d State: %s\n", frame->contacts[c].id, CONTACT_STATE_STRING[state]);

					//IDと座標
					//fprintf(stdout, "Contact ID: %d X: %f Y: %f\n", frame->contacts[c].id, frame->contacts[c].x_pos, frame->contacts[c].y_pos);

					x_cod = frame->contacts[c].x_pos;
					y_cod = frame->contacts[c].y_pos;
					//fprintf(stdout, "Contact ID: %d X: %f Y: %f\n", frame->contacts[c].id, x_cod, y_cod);

					//インチをミリメートルに変換
					x_p = frame->contacts[c].peak_x * 254.0f;
					y_p = frame->contacts[c].peak_y * 254.0f;
					fprintf(stdout, "X: %f Y: %f P: %f\n", x_p, y_p, frame->contacts[c].peak_force);


					//ある範囲内だけの認識
					//if (x_cod > 30.0f && x_cod < 70.0f && y_cod > 10.0f && y_cod < 50.0f) {

					//	//peakの圧力(g)
					//	fprintf(stdout, "X: %f Y: %f P: %f\n", frame->contacts[c].peak_x, frame->contacts[c].peak_y, frame->contacts[c].peak_force);

					//}



					////Turn on LED for CONTACT_START
					//if (state == CONTACT_START) {
					//	//1つのLEDの輝度を更新する,100は明るさ
					//	senselSetLEDBrightness(handle, frame->contacts[c].id, 100);
					//}
					////Turn off LED for CONTACT_END
					//else if (state == CONTACT_END) {
					//	senselSetLEDBrightness(handle, frame->contacts[c].id, 0);
					//}
				}
				//fprintf(stdout, "\n");
			}
		}
	}
	return 0;
}


